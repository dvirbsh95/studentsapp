//
//  FirebaseErrorExtension.swift
//  Train Your Brain
//
//  Created by admin on 01/01/2021.
//  Copyright © 2021 admin. All rights reserved.
//

import Foundation
import Firebase

extension String {
    var isNotEmpty: Bool{
        return !isEmpty
    }
}

extension UIViewController {
    func handleFirebaseError(_ error: Error){
        if let errorCode = AuthErrorCode(rawValue: error._code) {
            let alert = UIAlertController(title: "Error", message: errorCode.errorMessage , preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension AuthErrorCode {
    var errorMessage: String {
        switch self {
            
        case .emailAlreadyInUse:
            return "The email is already in use with another account."
        
        case .invalidEmail, .invalidSender, .invalidRecipientEmail:
                   return "Please enter a valid email address."
            
        case .networkError:
            return "Network Error. Pleas try again later."
            
        case .userNotFound:
                 return "Account not found for the specified user. Please cheack and try again."
                 
             case .userDisabled:
                 return "Your account has been disabled. Please contact with our support."
            
        case .weakPassword:
            return "Tour password is too weak. The password nust contain at least 8 character long or more."
            
        case .wrongPassword:
            return "Your eamil or password went wrong."
            
        default:
            return "Sorry, something went wrong. Please try again later"
        }
    }
}
