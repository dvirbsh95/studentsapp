//
//  ArticleViewCell.swift
//  studentsapp
//
//  Created by admin on 11/01/2021.
//  Copyright © 2021 admin. All rights reserved.
//

import UIKit

class ArticleViewCell: UITableViewCell {
    
    
    @IBOutlet weak var articleID: UILabel!
    @IBOutlet weak var articleTitle: UILabel!
    @IBOutlet weak var articlePublisher: UILabel!
    @IBOutlet weak var articleDate: UILabel!
    @IBOutlet weak var logoImageURL: UIImageView!
    @IBOutlet weak var articleContent: UILabel!
    
//    @IBOutlet weak var articleTitle: UILabel!
//    @IBOutlet weak var articlePublisher: UILabel!
//    @IBOutlet weak var articleDate: UILabel!
//    @IBOutlet weak var articleID: UILabel!
//    @IBOutlet weak var logoImageURL: UIImageView!
//

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
