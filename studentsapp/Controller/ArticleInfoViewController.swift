//
//  ArticleInfoViewController.swift
//  studentsapp
//
//  Created by admin on 11/01/2021.
//  Copyright © 2021 admin. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import JGProgressHUD

class ArticleInfoViewController: UIViewController {
    private let spinner = JGProgressHUD(style: .dark)
    var article:Article?
    
    @IBOutlet weak var articleID: UILabel!
    @IBOutlet weak var articleTitle: UILabel!
    @IBOutlet weak var articlePublisher: UILabel!
    @IBOutlet weak var articleDate: UILabel!
    @IBOutlet weak var articleContent: UILabel!
    @IBOutlet weak var removeBtn: UIButton!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBOutlet weak var videoURL: UILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        activity.isHidden = true;
        // Do any additional setup after loading the view.
        videoURL.isHidden = true
        
        removeBtn.addTarget(self, action: #selector(removeBtnClicked), for: .touchUpInside)
        articlePublisher.text = article?.publisherName
        articlePublisher.textColor = .white
        
        articleTitle.text = article?.title
        articleTitle.textColor = .link
        
        articleDate.text = article?.date
        articleDate.textColor = .systemYellow
        
        articleContent.text = article?.content
        articleContent.textColor = .white
        
        articleID.text = article?.id
        articleID.textColor = .white
        
        videoURL.text = article?.videoURL
        print("Video URL:  \(videoURL)")
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        
        let player = AVPlayer(url: URL(fileURLWithPath: Bundle.main.path(forResource: videoURL.text, ofType: "m4v")!))
        
        let layer = AVPlayerLayer(player: player)
        layer.frame = view.bounds
        view.layer.addSublayer(layer)
        
        player.play()
    }
    
    @objc  func removeBtnClicked(_ sender: Any) {
        Model.instance.deleteArticle(id: self.articleID.text ?? "", callback: {
            print("delete end")
        })
        
        spinner.show(in: view)

        self.dismissView()
        DispatchQueue.main.async {
            self.spinner.dismiss()
        }
        
        activity.isHidden = false;
    }

    
    func dismissView(){
        self.navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
}


