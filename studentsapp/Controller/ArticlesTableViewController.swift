//
//  ArticlesTableViewController.swift
//  studentsapp
//
//  Created by admin on 11/01/2021.
//  Copyright © 2021 admin. All rights reserved.
//

import UIKit
import Kingfisher

class ArticlesTableViewController: UITableViewController {
    
    var data = [Article]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.addTarget(self, action: #selector(reloadData), for: .valueChanged)
        
        ModelEvents.ArticleDataEvent.observe {
            self.reloadData();
        }
        
        self.refreshControl?.beginRefreshing()
        reloadData();
    }
    
    @objc func reloadData(){
        if         self.refreshControl?.isRefreshing == false {
            self.refreshControl?.beginRefreshing()
        }
        Model.instance.getAllArticles{ (_data:[Article]?) in
            if(_data != nil){
                self.data = _data!;
                self.tableView.reloadData();
            }
        }
        self.refreshControl?.endRefreshing()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:ArticleViewCell = tableView.dequeueReusableCell(withIdentifier: "ArticleCell", for: indexPath) as! ArticleViewCell
        // Configure the cell...
        
        let article = data[indexPath.row]
        cell.articleTitle.text = article.title
        cell.articleTitle.textColor = .link
        
        cell.articlePublisher.text = article.publisherName
        cell.articlePublisher.textColor = .green
        
        cell.articleID.text = article.id
        cell.articleID.textColor = .link
        
        cell.articleDate.text = String("\(article.date)")
        cell.articleDate.textColor = .systemYellow
        
        cell.articleContent.text = article.content
        
         cell.logoImageURL.image = UIImage(named: "css")
        
        if(article.logoImageURL != ""){
            let url = URL(string: article.logoImageURL);
            cell.logoImageURL.kf.setImage(with: url)
        }
        
        cell.logoImageURL.tintColor = .white
        cell.logoImageURL.layer.masksToBounds = true
        cell.logoImageURL.layer.cornerRadius = cell.logoImageURL.frame.size.width / 2.0
        cell.logoImageURL.layer.backgroundColor = UIColor.black.cgColor
        cell.logoImageURL.contentMode = .scaleAspectFit
        
        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
            let article = data[indexPath.row]
            let editAction = UITableViewRowAction(style: .default, title: "Edit", handler: { (action,indexPath) in
                self.updateAction(article: article, indexPath: indexPath)
                
            })
            
            editAction.backgroundColor = .blue
            return [editAction]
        }
        
        private func updateAction(article: Article, indexPath: IndexPath ){
            let alert = UIAlertController(title: "Update", message: "Update Article content", preferredStyle: .alert)
            let saveAction = UIAlertAction(title: "Save", style: .default, handler: { (action) in
                guard let textField = alert.textFields?.first else {return}
                if let textToEdit = textField.text {
                    if textToEdit.isEmpty {return}
                    article.content = textToEdit
                    Model.instance.addArticle(article: article)
                    self.tableView?.reloadRows(at: [indexPath], with: .automatic)
                }
                    
                    
                else{return}
            })
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .default)
            
            alert.addTextField()
            guard let textField = alert.textFields?.first else {return}
            textField.placeholder = "Edit Article Contnet"
            alert.addAction(saveAction)
            alert.addAction(cancelAction)
            present(alert, animated: true)
        }
        


     //MARK: - Navigation

     //    In a storyboard-based application, you will often want to do a little preparation before navigation
        var selected:Article?
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            if(segue.identifier == "ArticleInfoSegue"){
                let vc:ArticleInfoViewController = segue.destination as! ArticleInfoViewController
                vc.article = selected
            }
        }

        override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            selected = data[indexPath.row]
            performSegue(withIdentifier: "ArticleInfoSegue", sender: self)
        }
        
        @IBAction func backFromCancleLogin(segue:UIStoryboardSegue){}
    
}
