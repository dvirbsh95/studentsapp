//
//  NewArticleViewController.swift
//  studentsapp
//
//  Created by admin on 11/01/2021.
//  Copyright © 2021 admin. All rights reserved.
//

import UIKit
import JGProgressHUD

class NewArticleViewController: UIViewController, UIImagePickerControllerDelegate , UINavigationControllerDelegate {
    
    var article: Article?
    
    @IBOutlet weak var articleImage: UIImageView!
    @IBOutlet weak var articleTitle: UITextField!
    @IBOutlet weak var articleDate: UITextField!
    @IBOutlet weak var publisherName: UITextField!
    @IBOutlet weak var articleContent: UITextField!
    
    @IBOutlet weak var picBtn: UIButton!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBOutlet weak var videoURL: UITextField!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        activity.isHidden = true;
        articleContent.frame.size.height = 100
        
    }
    var selectedImage:UIImage?
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    @IBAction func save(_ sender: Any) {
        if let selectedImage = selectedImage{
            activity.isHidden = false;
            submitBtn.isEnabled = false;
            picBtn.isEnabled = false;
            Model.instance.saveImage(image: selectedImage, imageName: ""){ (url) in
                let article = Article(id:UUID().uuidString, title: self.articleTitle.text!, publisherName: self.publisherName.text!, content: self.articleContent.text!, date: self.articleDate.text!, logoImageURL: url, videoURL: self.videoURL.text!)
                Model.instance.addArticle(article:article);
                self.navigationController?.popViewController(animated: true)
            }
        }
    } 
    
    @IBAction func pic(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(
        UIImagePickerController.SourceType.photoLibrary) {
         let imagePicker = UIImagePickerController()
            imagePicker.delegate = self;
         imagePicker.sourceType =
            UIImagePickerController.SourceType.photoLibrary;
         imagePicker.allowsEditing = true
         self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        selectedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        self.articleImage.image = selectedImage;
        dismiss(animated: true, completion: nil);
    }

}

