//
//  UserModel.swift
//  studentsapp
//
//  Created by admin on 09/01/2021.
//  Copyright © 2021 admin. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase

class UserModel {
    let modelFirebase = ModelFirebase()
    let modelSqlForUsers = ModelSqlForUsers.instance
    
    static let instance = UserModel()
    
    private init(){ }
    
    func addUser(user: User) -> () {
        modelFirebase.ref.child("users").child(user.Id).setValue(user.toJson())
    }
    
   func getUser(uid:String, callback: @escaping (User?)->Void){
        //get the local last update date
        var lud = modelSqlForUsers.getLastUpdateDate(name: "USERS");
        let oldLud = lud
        
        //get the cloud updates since the local update date
        modelFirebase.getUser(since:lud, uid: uid) { (user) in
            //insert update to the local db
            
            if (user != nil){
                self.modelSqlForUsers.addUser(user: user!)
                
                if user!.lastUpdate != nil && user!.lastUpdate! > lud {
                    lud = user!.lastUpdate!
                }
                
                if (lud > oldLud){
                    //update the students local last update date
                    self.modelSqlForUsers.setLastUpdate(name: "USERS", lastUpdated: lud)
                }
            }
            
            // get the complete student list
            let finalData = self.modelSqlForUsers.getUserbyId(uid: uid)
            callback(finalData);
        }
    }
}
