//
//  Post.swift
//  studentsapp
//
//  Created by admin on 06/01/2021.
//  Copyright © 2021 admin. All rights reserved.
//

import Foundation

class Post {
    
    var postID:String
    var subject:String = ""
    var studentName:String = ""
    var postContent:String = ""
    var imageURL:String = ""
    var lastUpdate:Int64 = 0
    
    init(postID:String, subject:String, studentName:String, imageURL:String){
        self.postID = postID
        self.subject = subject
        self.studentName = studentName
        self.imageURL = imageURL
    }
    
    init(id:String){
           self.id = id
         
       }
    
    init(json:[String:Any]){
        self.id = json["id"] as! String;
        self.name = json["name"] as! String;
        self.post = json["post"] as! String;
        self.email = json["email"] as! String;
        self.avatar = json["avatar"] as! String;
        let ts = json["lastUpdate"] as! Timestamp
        self.lastUpdate = ts.seconds
    }
    
    
    func toJson() -> [String:Any] {
        var json = [String:Any]();
        json["id"] = id
        json["post"] = post
        json["name"] = name
        json["email"] = email
        json["avatar"] = avatar
        json["lastUpdate"] = FieldValue.serverTimestamp()
        return json;
    }
    
    
    
    
}
