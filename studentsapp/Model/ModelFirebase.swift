//
//  ModelFirebase.swift
//  studentsapp
//
//  Created by admin on 29/12/2020.
//  Copyright © 2020 admin. All rights reserved.
//

import Foundation
import  Firebase

class ModelFirebase{
    
    var ref: DatabaseReference!
    
    init() {
        ref = Database.database(url: "https://studentsapp-8988c-default-rtdb.firebaseio.com/").reference()
    }
    
    func addArticle(article:Article){
        let db = Firestore.firestore();
        
        // Add a new document with a generated ID
        //    var ref: DocumentReference? = nil
        let json = article.toJson();
        db.collection("articles").document(article.id).setData(json){
            error in
            if let error = error {
                print("Error writing document: \(error)")
            } else {
                print("Document successfully writen!")
                ModelEvents.ArticleDataEvent.article();
            }
        }
    }
    
    
    func getUser(since:Int64, uid:String, callback: @escaping (User?)->Void){
        ref.child("users")
            //.queryOrdered(byChild: "lastUpdate").queryStarting(atValue: since + 1)
            .observe(.value, with:{ (snapshot: DataSnapshot) in
                
                if let snapshot = snapshot.children.allObjects as? [DataSnapshot] {
                    var data = [User]();
                    
                    for snap in snapshot {
                        if let postDict = snap.value as? Dictionary<String, AnyObject> {
                            let user = User(json: postDict)
                            data.append(user)
                        } else {
                            print("failed to convert")
                        }
                    }
                    
                    let user = data.filter{u in return u.Id == uid}.first
                    callback(user)
                }
            })
    }
    
    func saveImage(image:UIImage, imageName:String, callback:@escaping (String)->Void) {
        FirebaseStorage.saveImage(image: image, imageName: imageName, callback: callback)
    }
    
    func deleteArticle(id: String, callback: @escaping ()->Void){
        let db = Firestore.firestore()
        db.collection("articles").document(id).delete() { error in
            if let error = error {
                print("Error removing document: \(error)")
            } else {
                print("Document successfully removed!")
            }
            ModelEvents.ArticleDataEvent.article();
        }
        
    }
    
    
    func getAllArticles(since:Int64, callback: @escaping ([Article]?)->Void){
        let db = Firestore.firestore()
        db.collection("articles").order(by: "lastUpdate").start(at: [Timestamp(seconds: since, nanoseconds: 0)]).getDocuments { (querySnapshot, error) in
            if let error = error {
                print("Getting documents Failed: \(error)")
                callback(nil);
            } else {
                var data = [Article]();
                for document in querySnapshot!.documents {
                    if let ts = document.data()["lastUpdate"] as? Timestamp{
                        let tsData = ts.dateValue();
                        print("\(tsData)")
                        let tsDouble = tsData.timeIntervalSince1970
                        print("\(tsDouble)")
                        
                    }
                    print(Article(json: document.data()).id);
                    data.append(Article(json: document.data()));
                }
                callback(data);
            }
        };
    }
}
