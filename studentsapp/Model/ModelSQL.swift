import Foundation

class ModelSQL{
    
    var database: OpaquePointer? = nil
    
    
    init()  {
        let dbFileName = "database2.db"
        if let dir = FileManager.default.urls(for: .documentDirectory, in:
            .userDomainMask).first{
            let path = dir.appendingPathComponent(dbFileName)
            if sqlite3_open(path.absoluteString, &database) != SQLITE_OK {
                print("Failed to open db file: \(path.absoluteString)")
                return
            }
            print("Database Path: \(path)")
        }
        
        create()
    }
    
    func create() {
        var errormsg: UnsafeMutablePointer<Int8>? = nil
        var result = sqlite3_exec(database, "CREATE TABLE IF NOT EXISTS ARTICLES (ARTICLE_ID TEXT PRIMARY KEY, TITLE TEXT, PUBLISHER_NAME TEXT, CONTENT TEXT, DATE TEXT,LOGO_IMAGE_URL TEXT, VIDEO_URL TEXT)", nil, nil, &errormsg);
        
        if(result != 0){
            print("error creating table");
            return
        }
        print("Articles Table creation completed successfully")
        
        result = sqlite3_exec(database, "CREATE TABLE IF NOT EXISTS LAST_UPDATE_DATE (ARTICLE_ID TEXT PRIMARY KEY, LAST_UPDATE_DATE NUMBER)",nil, nil, &errormsg);
        if (result != 0) {
            print("error in creating table");
            return
        }
        print("Last Update Date  Table creation completed successfully")
        
    }
    
    func addArticle(article:Article){
        var sqlite3_stmt: OpaquePointer? = nil
        if (sqlite3_prepare_v2(database,"INSERT OR REPLACE INTO ARTICLES(ARTICLE_ID, TITLE , PUBLISHER_NAME , CONTENT,DATE, LOGO_IMAGE_URL, VIDEO_URL) VALUES (?,?,?,?,?,?,?);",-1,&sqlite3_stmt,nil) == SQLITE_OK){
            let id = article.id.cString(using: .utf8)
            let title = article.title.cString(using: .utf8)
            let publisher_name = article.publisherName.cString(using: .utf8)
            let content = article.content.cString(using: .utf8)
            let date = article.date.cString(using: .utf8)
            let logo_image_url = article.logoImageURL.cString(using: .utf8)
            let video_url = article.videoURL.cString(using: .utf8)
            
            sqlite3_bind_text(sqlite3_stmt, 1, id,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 2, title,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 3, publisher_name,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 4, content,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 5, date,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 6, logo_image_url,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 7, video_url,-1,nil);
            if(sqlite3_step(sqlite3_stmt) == SQLITE_DONE){
                print("new article row added succefully")
            }
        }
    }
    
    func deleteArticle(id: String){
        var sqlite3_stmt: OpaquePointer? = nil
        if (sqlite3_prepare_v2(database,"DELETE FROM ARTICLES WHERE ARTICLE_ID = ?;",-1, &sqlite3_stmt,nil) == SQLITE_OK){

            let _id = id.cString(using: .utf8)

            sqlite3_bind_text(sqlite3_stmt, 1, _id,-1,nil);

            if(sqlite3_step(sqlite3_stmt) == SQLITE_DONE){
                print("article row deleted succefully")
            }
        }
    }

    
    func getAllArticles()->[Article]{
        var sqlite3_stmt: OpaquePointer? = nil
        var data = [Article]()
        if (sqlite3_prepare_v2(database,"SELECT * from ARTICLES;",-1,&sqlite3_stmt,nil) == SQLITE_OK){
            while(sqlite3_step(sqlite3_stmt) == SQLITE_ROW){
                let id = String(cString:sqlite3_column_text(sqlite3_stmt,0)!)
                let title = String(cString:sqlite3_column_text(sqlite3_stmt,1)!)
                let publisher_name = String(cString:sqlite3_column_text(sqlite3_stmt,2)!)
                let content = String(cString:sqlite3_column_text(sqlite3_stmt,3)!)
                let date = String(cString:sqlite3_column_text(sqlite3_stmt,4)!)
                let loge_image_url = String(cString:sqlite3_column_text(sqlite3_stmt,5)!)
                let video_url = String(cString:sqlite3_column_text(sqlite3_stmt,6)!)
                
                data.append(Article(id: id, title: title, publisherName: publisher_name, content: content, date: date, logoImageURL: loge_image_url, videoURL: video_url))
            }
        }
        sqlite3_finalize(sqlite3_stmt)
        return data
    }
    
    func getLastUpdateDate(article_id:String) -> Int64 {
        var sqlite3_stmt: OpaquePointer? = nil
        var last_update_date: Int64 = 0;
        
        if (sqlite3_prepare_v2(database,"SELECT * FROM LAST_UPDATE_DATE where article_id = ?;",-1,&sqlite3_stmt,nil) == SQLITE_OK){
            sqlite3_bind_text(sqlite3_stmt, 1, article_id.cString(using: .utf8),-1,nil);
            
            if(sqlite3_step(sqlite3_stmt) == SQLITE_ROW){
                last_update_date = sqlite3_column_int64(sqlite3_stmt, 1)
            }
        }
        sqlite3_finalize(sqlite3_stmt)
        return last_update_date
    }
    
    func setLastUpdateDate(article_id:String, last_update_date: Int64){
        var sqlite3_stmt: OpaquePointer? = nil
        
        if (sqlite3_prepare_v2(database,"INSERT OR REPLACE INTO LAST_UPDATE_DATE(ARTICLE_ID, LAST_UPDATE_DATE) VALUES (?,?);",-1,&sqlite3_stmt,nil) == SQLITE_OK){
            sqlite3_bind_text(sqlite3_stmt, 1, article_id.cString(using: .utf8),-1,nil);
            sqlite3_bind_int64(sqlite3_stmt, 2, last_update_date)
            
            if(sqlite3_step(sqlite3_stmt) == SQLITE_DONE){
                print("new last update date row added succefully")
            }
        }
    }
    

}
