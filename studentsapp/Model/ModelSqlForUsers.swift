//
//  ModelSqlForUsers.swift
//  studentsapp
//
//  Created by admin on 09/01/2021.
//  Copyright © 2021 admin. All rights reserved.
//

import Foundation

class ModelSqlForUsers{
    var database: OpaquePointer? = nil
    
    static let instance = ModelSqlForUsers()
    
    private init(){
        connect()
    }
    
    private func connect() {
        let dbFileName = "database2.db"
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first{
            let path = dir.appendingPathComponent(dbFileName)
            if sqlite3_open(path.absoluteString, &database) != SQLITE_OK {
                print("Failed to open db file: \(path.absoluteString)")
                return
            }
        }
        create()
    }

    // MARK: CREATE TABLES
    private func create(){
        var errormsg: UnsafeMutablePointer<Int8>? = nil
        var res = sqlite3_exec(database, "CREATE TABLE IF NOT EXISTS USERS (ID TEXT PRIMARY KEY, NAME TEXT, PICTURE TEXT)", nil, nil, &errormsg);

        if(res != 0){
            print("error creating users table");
            return
        }
      
        
        res = sqlite3_exec(database, "CREATE TABLE IF NOT EXISTS LAST_UPDATE_DATE (NAME TEXT PRIMARY KEY, DATE DOUBLE)", nil, nil, &errormsg);
        if(res != 0){
            print("error creating last update date table");
            return
        }
    }
     
    // MARK: LAST UPDATE DATE
    func setLastUpdate(name:String, lastUpdated:Int64){
        var sqlite3_stmt: OpaquePointer? = nil
        if (sqlite3_prepare_v2(database,"INSERT OR REPLACE INTO LAST_UPDATE_DATE( NAME, DATE) VALUES (?,?);",-1, &sqlite3_stmt,nil) == SQLITE_OK){

            sqlite3_bind_text(sqlite3_stmt, 1, name,-1,nil);
            sqlite3_bind_int64(sqlite3_stmt, 2, lastUpdated);
            if(sqlite3_step(sqlite3_stmt) == SQLITE_DONE){
                print("new row added succefully")
            }
        }
        sqlite3_finalize(sqlite3_stmt)
    }
    
    func getLastUpdateDate(name:String)->Int64{
        var date:Int64 = 0;
        var sqlite3_stmt: OpaquePointer? = nil
        if (sqlite3_prepare_v2(database,"SELECT * from LAST_UPDATE_DATE where NAME like ?;",-1,&sqlite3_stmt,nil)
            == SQLITE_OK){
            
            sqlite3_bind_text(sqlite3_stmt, 1, name,-1,nil);

            if(sqlite3_step(sqlite3_stmt) == SQLITE_ROW){
                date = Int64(sqlite3_column_int64(sqlite3_stmt,1))
            }
        }
        sqlite3_finalize(sqlite3_stmt)
        return date
    }

    // MARK: USERS
    
    func addUser(user: User){
        var sqlite3_stmt: OpaquePointer? = nil
        if (sqlite3_prepare_v2(database,"INSERT OR REPLACE INTO USERS( ID, NAME, PICTURE) VALUES (?,?,?);",-1, &sqlite3_stmt,nil) == SQLITE_OK){

            let id = user.Id.cString(using: .utf8)
            let name = user.Name.cString(using: .utf8)
            let picture = user.Picture.cString(using: .utf8)
            
            sqlite3_bind_text(sqlite3_stmt, 1, id,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 2, name,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 3, picture,-1,nil);
            
            if(sqlite3_step(sqlite3_stmt) == SQLITE_DONE){
                print("user added succefully")
            }
        }
    }
    
    func getUserbyId(uid:String)-> User? {
        var sqlite3_stmt: OpaquePointer? = nil
        var data = [User]()
        
        if (sqlite3_prepare_v2(database,"SELECT * from USERS;",-1,&sqlite3_stmt,nil)
            == SQLITE_OK){
            
            //sqlite3_bind_text(sqlite3_stmt, 1, uid,-1,nil);
            
            while(sqlite3_step(sqlite3_stmt) == SQLITE_ROW){

                let id = String(cString:sqlite3_column_text(sqlite3_stmt,0)!)
                let name = String(cString:sqlite3_column_text(sqlite3_stmt,1)!)
                let picture = String(cString:sqlite3_column_text(sqlite3_stmt,2)!)
                
                let user = User(Id: id, name: name, Picture: picture)
                                
                data.append(user)
            }
        }
        
        sqlite3_finalize(sqlite3_stmt)
        
        let users = data.filter{user in return user.Id == uid}
        return users.first
    }
    
    
    
    // MARK: DROP TABLES
    func drop(){
        var errormsg: UnsafeMutablePointer<Int8>? = nil
        var res = sqlite3_exec(database, "DROP TABLE IF EXISTS USERS;", nil, nil, &errormsg);

        if(res != 0){
            print("error creating table");
            return
        }
        
        res = sqlite3_exec(database, "DROP TABLE IF EXISTS LAST_UPDATE_DATE;", nil, nil, &errormsg);
        if(res != 0){
            print("error creating table");
            return
        }
    }
}
