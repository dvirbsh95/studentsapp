//
//  Model.swift
//  iosProject
//
//  Created by Dvir Ben Shlush on 17/12/2020.
//  Copyright © 2020 Shahar Levi. All rights reserved.
//

import Foundation
import UIKit


class Model {
    static let instance = Model();
    
    var modelSQL:ModelSQL = ModelSQL()
    var modelFirebase:ModelFirebase = ModelFirebase()
    
    private init() {}
    
    
    
    func addArticle(article:Article) {
        modelFirebase.addArticle(article:article)
    }
    
    
    func getAllArticles(callback:@escaping ([Article]?)->Void) {
        let lud = modelSQL.getLastUpdateDate(article_id: "ARTICLES")
        modelFirebase.getAllArticles(since: lud){ (articleData) in
            var localLud:Int64 = 0
            for article in articleData!{
                self.modelSQL.addArticle(article:article)
                if(article.lastUpdate > localLud ){
                    localLud = article.lastUpdate
                }
            }
            self.modelSQL.setLastUpdateDate(article_id: "ARTICLES", last_update_date: localLud)
            let completData = self.modelSQL.getAllArticles()
            callback(completData);
            
        };
    }
    
    
    func deleteArticle(id: String, callback:@escaping ()->Void) {
        modelSQL.deleteArticle(id: id)
        modelFirebase.deleteArticle(id: id, callback: callback)
    }
    
    func saveImage(image: UIImage,imageName:String, callback:@escaping (String)->Void) {
        FirebaseStorage.saveImage(image: image, imageName: imageName, callback: callback)
    }
    
    var logedIn = false
    func isLogedIn() -> Bool {
        return logedIn
    }
    
    func login(email:String, pass:String, callback: (Bool)->Void) {
        logedIn = true
        callback(true)
    }
    
    func logout(callback: (Bool)->Void) {
        logedIn = false
    }
    
    func register(user:String, email:String, pass:String, callback: (Bool)->Void) {
        logedIn = true
        callback(true)
    }
    
}


class ModelEvents{
    static let UserDataEvent = EventNotificationBase(eventName: "com.dvir.UserDataEvent");
    static let ArticleDataEvent = EventNotificationBase(eventName: "com.dvir.ArticleDataEvent");
    static let LoggingStateChangeEvent = EventNotificationBase(eventName: "com.dvir.LoggingStateChangeEvent");
    static let GPSUpdateEvent = ModelEventsTemplateWithArgs<String>(eventName: "com.dvir.GPSUpdateEvent");  // this is example!!
    
    private init(){}
}

class EventNotificationBase  {
    
    let eventName:String;
    
    init(eventName:String) {
        self.eventName = eventName;
    }
    func observe(callback:@escaping()->Void) {
        NotificationCenter.default.addObserver(forName: NSNotification.Name(eventName), object: nil, queue: nil) {
            (data) in
            callback();
        }
    }
    
    func article() {
        NotificationCenter.default.post(name: NSNotification.Name(eventName), object: self, userInfo: nil)
    }
}

class ModelEventsTemplateWithArgs<T>  {
    
    let eventName:String;
    
    init(eventName:String) {
        self.eventName = eventName;
    }
    func observe(callback:@escaping(T)->Void) {
        NotificationCenter.default.addObserver(forName: NSNotification.Name(eventName), object: nil, queue: nil) {
            (data) in
            let d:T = data.userInfo!["data"] as! T
            callback(d);
        }
    }
    
    
    func post(data:T) {
        NotificationCenter.default.post(name: NSNotification.Name(eventName), object: self, userInfo: ["data":data])
    }
}
