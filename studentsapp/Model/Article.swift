//
//  Article.swift
//  studentsapp
//
//  Created by admin on 11/01/2021.
//  Copyright © 2021 admin. All rights reserved.
//

import Foundation
import Firebase

class Article {

    var id:String = ""
    var title:String = ""
    var publisherName:String = ""
    var content:String = ""
    var date:String = ""
    var logoImageURL:String = ""
    var videoURL:String = ""
    var lastUpdate:Int64 = 0
    
    init(id:String, title:String, publisherName:String, content:String, date:String, logoImageURL:String, videoURL:String ){
        self.id = id
        self.title = title
        self.publisherName = publisherName
        self.content = content
        self.date = date
        self.logoImageURL = logoImageURL
        self.videoURL = videoURL
    }
    
    
    init(json:[String:Any]){
        self.id = json["id"] as! String;
        self.title = json["title"] as! String;
        self.publisherName = json["publisherName"] as! String;
        self.content = json["content"] as! String;
        self.date = json["date"] as! String;
        self.logoImageURL = json["logoImageURL"] as! String;
        self.videoURL = json["videoURL"] as! String;
        let ts = json["lastUpdate"] as! Timestamp
        self.lastUpdate = ts.seconds
    }
    
    
    func toJson() -> [String:Any] {
        var json = [String:Any]();
        json["id"] = id
        json["title"] = title
        json["publisherName"] = publisherName
        json["content"] = content
        json["date"] = date
        json["logoImageURL"] = logoImageURL
        json["videoURL"] = videoURL
        json["lastUpdate"] = FieldValue.serverTimestamp()
        return json;
    }
    
}
