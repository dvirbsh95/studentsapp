//
//  ProfileViewController.swift
//  studentsapp
//
//  Created by admin on 07/01/2021.
//  Copyright © 2021 admin. All rights reserved.
//

import UIKit
import FirebaseAuth
import Kingfisher
import FirebaseStorage


class ProfileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var logoutBtn: UIButton!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var editBtn: UIButton!
    
    
    let model = UserModel.instance
    var uid:String = ""
    var user: User?
    var selectedImage:UIImage?;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let firebaseUser = Auth.auth().currentUser {
            uid = firebaseUser.uid
            print("this is your id \(uid)")
      
        }
                
        reloadData()
        // Do any additional setup after loading the view.
        logoutBtn.addTarget(self, action: #selector(logoutClicked), for: .touchUpInside)
         editBtn.addTarget(self, action: #selector(EditProfilePicture), for: .touchUpInside)
    }
    
    
    func reloadData(){
        
        model.getUser(uid: self.uid){ (user:User?) in
            if (user != nil) {
                self.user = user
                self.userName.text = user?.Name ?? "Profile"
                print("test")
                
                let url = URL(string: user?.Picture ?? "")
                self.profileImage.kf.setImage(with: url)
                

                print("reloadedata  \(String(describing: user?.Name))")
            }
            else{ print("reloadedata is nil") }
        }

    }
    
    
    @IBAction func EditProfilePicture(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(
            UIImagePickerController.SourceType.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self;
            imagePicker.sourceType =
                UIImagePickerController.SourceType.photoLibrary;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
            print(self.user?.Name)

        }
        
        
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        selectedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage;
        self.profileImage.image = selectedImage;
        
        // save user
        UserModel.instance.modelFirebase.saveImage(image: selectedImage!, imageName: self.uid) { (url) in
            self.user?.Picture = url
            self.model.addUser(user: self.user!)
            self.navigationController?.popViewController(animated: true)
        }

        picker.dismiss(animated: true, completion: nil)
    }
    
     @objc func logoutClicked(){
         
         let actionSheet = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
         
         actionSheet.addAction(UIAlertAction(title: "Log Out", style: .destructive, handler: {[weak self] _ in
             guard let strongSelf = self else {return}
             
             do {
                 try FirebaseAuth.Auth.auth().signOut()
                 
                 let vc = newLoginViewController()
                 let nav = UINavigationController(rootViewController: vc)
                 nav.modalPresentationStyle = .fullScreen
                 
                 (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(nav)
             }
             catch{
                 print("Failed To Logout")
             }
         }))
         
         actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
         present(actionSheet, animated: true)
     }
}
